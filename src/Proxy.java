public class Proxy implements Abstraction {

	private Abstraction abstraction;

	@Override
	public void operation() {
		System.out.println("Proxy is verifying access conditions");
		abstraction = new AbstractionImpl();
		abstraction.operation();
	}

}
