public class AbstractionImpl implements Abstraction {
	@Override
	public void operation() {
		System.out.println("Executing implementation operation");
	}
}
